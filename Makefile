CC := gcc
CFLAGS := \
	-std=c99 \
	-Wall \
	-Wextra \
	-Werror \
	-Wpedantic \
	-Wcast-align \
	-Wcast-qual \
	-Wmissing-declarations \
	-Wredundant-decls \
	-Wmissing-prototypes \
	-Wpointer-arith \
	-Wshadow \
	-Wstrict-prototypes \
	-Wmissing-field-initializers \
	-fno-common

.PHONY: all
all: example_simple_stdout

example_simple_stdout: examples/simple_stdout.c twig.h
	$(CC) $(CFLAGS) -o example_simple_stdout examples/simple_stdout.c

example_multiple_sinks: examples/multiple_sinks.c twig.h
	$(CC) $(CFLAGS) -o example_multiple_sinks examples/multiple_sinks.c
