#ifndef TWIG_H
#define TWIG_H

#include <stdarg.h>     /* va_list, va_start(), va_end() */
#include <stddef.h>     /* NULL */

typedef enum {
  TWIG_FDEBUG = 1 << 0,
  TWIG_FINFO  = 1 << 1,
  TWIG_FWARN  = 1 << 2,
  TWIG_FERROR = 1 << 3,
} twig_flag_t;

typedef enum {
  TWIG_LNONE  = 0,
  TWIG_LERROR = TWIG_LNONE  | TWIG_FERROR,
  TWIG_LWARN  = TWIG_LERROR | TWIG_FWARN,
  TWIG_LINFO  = TWIG_LWARN  | TWIG_FINFO,
  TWIG_LDEBUG = TWIG_LINFO  | TWIG_FDEBUG,
  TWIG_LALL   = TWIG_LDEBUG,
} twig_level_t;

typedef enum {
  TWIG_ERRNO_OK = 0,
  TWIG_ERRNO_ERROR,
} twig_errno_t;

typedef struct {
  char *contents;
  size_t size;
} twig_buffer_t;

typedef struct {
  twig_flag_t flag;
  twig_buffer_t *buffer;
  char *label;
  char *sfile;
  int sline;
} twig_record_t;

typedef struct twig_sink_s twig_sink_t;

typedef twig_errno_t (*twig_write_f)(twig_sink_t *sink, twig_record_t *record);

struct twig_sink_s {
  void *dest;
  twig_write_f write;
  twig_level_t level;
};

typedef twig_errno_t (*twig_format_f)(void *data, twig_buffer_t **buffer, char *fmt, va_list varargs);

typedef struct {
  twig_sink_t **sinks;
  char *label;
  void *fmt_data;
  twig_format_f format;
} twig_emitter_t;

twig_errno_t
twig_emit(twig_emitter_t *emitter, char *sfile, int sline, twig_flag_t flag, char *fmt, ...);

#define twig_debug(emitter, fmt, ...) \
  twig_emit(emitter, __FILE__, __LINE__, TWIG_FDEBUG, fmt, __VA_ARGS__)

#define twig_info(emitter, fmt, ...) \
  twig_emit(emitter, __FILE__, __LINE__, TWIG_FINFO, fmt, __VA_ARGS__)

#define twig_warn(emitter, fmt, ...) \
  twig_emit(emitter, __FILE__, __LINE__, TWIG_FWARN, fmt, __VA_ARGS__)

#define twig_error(emitter, fmt, ...) \
  twig_emit(emitter, __FILE__, __LINE__, TWIG_FERROR, fmt, __VA_ARGS__)

#ifdef TWIG_IMPLEMENTATION

twig_errno_t
twig_emit(twig_emitter_t *emitter, char *sfile, int sline, twig_flag_t flag, char *fmt, ...)
{
  va_list varargs;
  twig_errno_t twig_errno;
  twig_record_t record = {
    .flag = flag,
    .label = emitter->label,
    .sfile = sfile,
    .sline = sline,
    .buffer = NULL,
  };

  va_start(varargs, fmt);
  twig_errno = emitter->format(emitter->fmt_data, &record.buffer, fmt, varargs);
  va_end(varargs);

  if (twig_errno != TWIG_ERRNO_OK) {
    return TWIG_ERRNO_ERROR;
  }
  for (twig_sink_t **iter = emitter->sinks; *iter != NULL; iter++) {
    twig_sink_t *sink = *iter;

    if ((sink->level & flag) == 0) {
      continue;
    }
    if (sink->write(sink, &record) != TWIG_ERRNO_OK) {
      return TWIG_ERRNO_ERROR;
    }
  }

  return TWIG_ERRNO_OK;
}

#endif /* TWIG_IMPLEMENTATION */

#endif /* TWIG_H */
