/* This example will log to:
 *  - stdout - debug, info, warn and error messages
 *  - syslog - warn and error messages
 */

#define TWIG_IMPLEMENTATION
#include "../twig.h"

#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <time.h>

twig_errno_t
stream_write(twig_sink_t *sink, twig_record_t *record);

twig_errno_t
syslog_write(twig_sink_t *sink, twig_record_t *record);

typedef struct {
  FILE *stream;
  twig_buffer_t buffer;
} fmt_data_t;

twig_errno_t
format_message(void *data, twig_buffer_t **buffer, char *fmt, va_list varargs);

int main(int argc, char **argv)
{
  (void)argc;
  (void)argv;

  // --------------------------------------------------------------------------
  fmt_data_t fmt_data = { 0 };
  fmt_data.stream = open_memstream(
    &fmt_data.buffer.contents,
    &fmt_data.buffer.size
  );

  openlog("twig", LOG_NDELAY | LOG_PID, LOG_USER);

  // --------------------------------------------------------------------------
  twig_sink_t sink_stdout = {
    .dest = stdout,
    .write = &stream_write,
    .level = TWIG_LDEBUG,
  };
  twig_sink_t sink_syslog = {
    .dest = NULL, /* syslog has global state */
    .write = &syslog_write,
    .level = TWIG_LWARN,
  };
  twig_sink_t *sinks[] = { &sink_stdout, &sink_syslog, NULL };
  twig_emitter_t logger = {
    .label = "root",
    .sinks = sinks,
    .fmt_data = (void *)&fmt_data,
    .format = &format_message,
  };

  // --------------------------------------------------------------------------
  twig_debug(&logger, "%s message! %d %c", "debug", 1, 'a');
  twig_info(&logger, "%s message! %d %c", "info", 2, 'b');
  twig_warn(&logger, "%s message! %d %c", "warn", 3, 'c');
  twig_error(&logger, "%s message! %d %c", "error", 4, 'd');

  // --------------------------------------------------------------------------
  closelog();
  fclose(fmt_data.stream);
  free(fmt_data.buffer.contents);

  return 0;
}

twig_errno_t
stream_write(twig_sink_t *sink, twig_record_t *record)
{
  static char flag_chars[] = {
    [TWIG_FDEBUG] = 'D',
    [TWIG_FINFO]  = 'I',
    [TWIG_FWARN]  = 'W',
    [TWIG_FERROR] = 'E',
  };
  int chars_printed = fprintf(
    sink->dest,
    "%c|%s| %s\n",
    flag_chars[record->flag],
    record->label,
    record->buffer->contents
  );

  if (chars_printed < 0) {
    return TWIG_ERRNO_ERROR;
  }
  return TWIG_ERRNO_OK;
}

twig_errno_t
syslog_write(twig_sink_t *sink, twig_record_t *record)
{
  (void)sink;

  static const int flag_to_syslog_level[] = {
    [TWIG_FDEBUG] = LOG_DEBUG,
    [TWIG_FINFO] = LOG_INFO,
    [TWIG_FWARN] = LOG_WARNING,
    [TWIG_FERROR] = LOG_ERR,
  };

  syslog(
    flag_to_syslog_level[record->flag],
    "[%s|%s:%d] %s",
    record->label,
    record->sfile,
    record->sline,
    record->buffer->contents
  );

  return TWIG_ERRNO_OK;
}

twig_errno_t
format_message(void *_data, twig_buffer_t **buffer, char *fmt, va_list varargs)
{
  fmt_data_t *fmt_data = (fmt_data_t *)_data;

  if (fseek(fmt_data->stream, 0, SEEK_SET) < 0) {
    return TWIG_ERRNO_ERROR;
  }
  if (vfprintf(fmt_data->stream, fmt, varargs) < 0) {
    return TWIG_ERRNO_ERROR;
  }
  if (fflush(fmt_data->stream) == EOF) {
    return TWIG_ERRNO_ERROR;
  }
  fmt_data->buffer.contents[fmt_data->buffer.size] = '\0';
  *buffer = &fmt_data->buffer;

  return TWIG_ERRNO_OK;
}
