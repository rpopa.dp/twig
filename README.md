# `twig`

Tiny, header-only logging library for C

## Contents

- [General](#general)
- [Usage](#usage)
  - [Logging levels](#logging-levels)
  - [Sinks](#sinks)
  - [Loggers](#loggers)
  - [Logging messages](#logging-messages)
- [Examples](#examples)

## General

What this library gives its users:
- support for multiple logging levels
- support for multiple sinks
- support for independent level handling for each sink
- support for custom string formatting
- support for logger labels

**NOTE:** this library is not thread safe. You either have to use the logger
from a single thread or add your own synchronization mecanisms.

**NOTE:** this library does not give a default implementation nor does it force
any specific message formatting and output destinations. The user is free to
decide what implementation will be used.

## Usage

In the main file include the library with the implementation:
```c
#define TWIG_IMPLEMENTATION
#include "twig.h"
```

In any other place where you need to log something, include the library only:
```c
#include "twig.h"
```

### Logging levels

The library has support for the following logging levels:
- debug
- info
- warning
- error

The library defines a constant for each level (`twig_level_t`):
```c
TWIG_LERROR /* only error messages will be logged */
TWIG_LWARN  /* only error and warning messages will be logged */
TWIG_LINFO  /* only error, warning and info messages will be logged */
TWIG_LDEBUG /* only error, warning, info and debug messages will be logged */
```

with additional constants:
```c
TWIG_LNONE  /* no messages will be logged */
TWIG_LALL   /* all messages will be logged */
```

The library also defines a level flag (`twig_flag_t`) for each level:
```c
TWIG_FDEBUG
TWIG_FINFO
TWIG_FWARN
TWIG_FERROR
```

`twig_level_t` is defined in terms of `twig_flag_t` by `OR`ing multiple flags
together. For example:
```c
TWIG_LWARN = TWIG_FERROR | TWIG_FWARN
```

A custom logging level can be used to select only specific messages for logging.
For example, to log only warning and debug messages:
```c
TWIG_FWARN | TWIG_FDEBUG
```

### Sinks

To define a sink you need:
- a destination. This can be anything: memory, file, socket, syslog, etc.
- a function that handles the action of writing a buffer to the destination.
  This function must follow the declaration defined by `twig_write_f`
- a logging level

For example, here we define a simple sink that logs the message to `stdout`:
```c
twig_sink_t sink_stdout = {
  .dest = stdout,
  .write = &stream_write,
  .level = TWIG_LDEBUG,
};

twig_errno_t
stream_write(twig_sink_t *sink, twig_record_t *record)
{
  if (fprintf(sink->dest, "%s", record->buffer->contents) < 0) {
    return TWIG_ERRNO_ERROR;
  }
  return TWIG_ERRNO_OK;
}
```

### Loggers

To define a logger you need:
- a label for the logger. Can be used to identify a specific logger in the
  log output
- a list of sinks that the logger will output to. The list should be `NULL`
  terminated
- a data pointer used for bookkeeping by the format function
- a function used to format the message. This function must follow the
  declaration defined by `twig_format_f`

For example, here we define a logger that will identify itself as `root`,
format the message using `format_message` and output to `sink_stdout`:
```c
twig_sink_t *sinks[] = { &sink_stdout, NULL };
twig_emitter_t logger = {
  .label = "root",
  .sinks = sinks,
  .fmt_data = (void *)&fmt_data,
  .format = &format_message,
};
```

### Logging messages

If using `printf`-like formatting, then it may look like this:

```c
twig_debug(&logger, "opened file: %s", file_path);
twig_error(&logger, "failed to open file: %s, errno: %d", file_path, errno);
```

If you happen to have a custom message formatting library (for example:
Python-like formatting), then it may look like this:

```c
twig_debug(&logger, "opened file: {}", file_path);
twig_error(&logger, "failed to open file: {}, errno: {}", file_path, errno);
```

### Examples

For more examples see the `examples/` directory
